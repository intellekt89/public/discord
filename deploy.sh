#!/usr/bin/env bash

ENV=$1
STATUS=$2
ROLE1=$3
ROLE2=$4
WEBHOOK=$5

case $STATUS in
    "success" )
    EMBED_COLOR="3066993"
    STATUS_MESSAGE="Passed"
    AVATAR="https://gitlab.com/intellekt89/public/discord/-/raw/main/gitlab.png?inline=false"
    ;;

    "failure" )
    EMBED_COLOR="15158332"
    STATUS_MESSAGE="Failed"
    AVATAR="https://gitlab.com/intellekt89/public/discord/-/raw/main/gitlab.png?inline=false"
    ;;
esac

COMMIT_SUBJECT="$(git log -1 "$CI_COMMIT_SHA" --pretty="%s")"
COMMIT_MESSAGE="$(git log -1 "$CI_COMMIT_SHA" --pretty="%b" | sed -E ':a;N;$!ba;s/\r{0,1}\n/\\n/g')"

# Get Role ID, type \@ROLENAME

WEBHOOK_DATA_DEPLOY_DEMO='{
  "username": "GitLab",
  "avatar_url": "'"${AVATAR}"'",
  "content": "'"${ROLE1}"' '"${ROLE2}"' '"${GITLAB_USER_NAME}"' развернул новые версии для теста https://demo.i-propusk.ru и https://demo1.i-propusk.ru",
  "embeds": [{
    "color": '$EMBED_COLOR',
    "title": "Изменения в новой версии: '"$COMMIT_SUBJECT"'",
    "description": "Подробности об изменениях: '"$COMMIT_MESSAGE"'"
  }]
 
}'

WEBHOOK_DATA_DEPLOY_PROD='{
  "username": "GitLab",
  "avatar_url": "'"${AVATAR}"'",
  "content": "'"${ROLE1}"' '"${ROLE2}"' '"${GITLAB_USER_NAME}"' развернул новую версию на проде",
  "embeds": [{
    "color": '$EMBED_COLOR',
    "title": "Изменения в новой версии: '"$COMMIT_SUBJECT"'",
    "description": "Подробности об изменениях: '"$COMMIT_MESSAGE"'"
  }]
 
}'

case $ENV in
    "demo" )
    WEBHOOK_DATA=${WEBHOOK_DATA_DEPLOY_DEMO}
    ;;

    "prod" )
    WEBHOOK_DATA=${WEBHOOK_DATA_DEPLOY_PROD}
    ;;
esac

(curl --fail --progress-bar -A "GitlabCI-Webhook" -H Content-Type:application/json -H X-Author:Dubalda#3996 -d "${WEBHOOK_DATA}" "${WEBHOOK}" \
  && echo -e "\\n[Webhook]: Successfully sent the webhook.") || echo -e "\\n[Webhook]: Unable to send webhook."
